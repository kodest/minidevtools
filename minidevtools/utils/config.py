"""config"""

YEAR = 2024
AUTHOR = "kodest"
VERSION = "0.0.1"
NAME = "minidevtools"
MAIL = "destrochloridium@gmail.com"
PROJECT_URL = "https://gitlab.com/kodest/minidevtools"
INFO_URL = "https://gitlab.com/kodest/minidevtools/-/raw/main/minidevtools/__init__.py?ref_type=heads"
